# JARPG (**J**ust **a**nother **r**ole **p**laying **g**ame)
[![License: AGPL-3.0-or-later](https://img.shields.io/badge/%E2%9A%96%EF%B8%8F-AGPL%203.0%20or%20later-green)](https://www.gnu.org/licenses/agpl.html)
[![State: Unstable](https://img.shields.io/badge/State-Unstable-yellow.svg)](https://codeberg.org/vivi90/python-vmc/issues)


# Requirements
 - **Unity**: `2021.3.11f1`


# Contribution

## Guidelines
Please follow the [C# Coding Conventions](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/coding-style/coding-conventions).
All filenames SHOULD follow the `PascalCasing` naming convention.
Unity terrain assets MUST use the `Terrain` suffix (e.g. `ExampleTerrain.asset`) 
and navigation mesh MUST use the `NavMesh` suffix (e.g. `ExampleNavMesh.asset`).

## How to Work with
1. Install [Unity](https://unity.com).
2. Install [Git](https://git-scm.com) and [Git LFS](https://git-lfs.github.com).
3. Run `git lfs install` once.
4. Configure your name and email address:
   ```bash
   git config user.name "Your Name"
   git config user.email "you@example.org"
   ```
5. Get the path of your [**UnityYAMLMerge** tool (a.k.a. **Smart merge**)](https://docs.unity3d.com/Manual/SmartMerge.html) 
   and configure your `unityyamlmerge` driver:
   ```bash
   git config merge.unityyamlmerge.name "UnityYAMLMerge"
   git config merge.unityyamlmerge.driver "'D:/Unity/Editor/Data/Tools/UnityYAMLMerge.exe' merge --force --fallback none %O %B %A %P"
   ```
   ***Use your own path instead!***
6. Configure all git hooks:
   ```bash
   git config core.hooksPath .githooks
   ```
7. Configure your [credentials](https://docs.codeberg.org/security/ssh-key).
8. [Fork this repository](https://codeberg.org/repo/fork/66520) and work with your fork.
9. After finishing your work, create a [pull request](https://codeberg.org/the-game-game/jarpg/pulls).


## Project state meanings
 * **Unstable:** Still an *possibly* broken draft or breaking issues are known.
 * **Refactoring:** Structural and technical changes have currently priority.
 * **Stable:** Everything seems working as expected.
 * **Fixes only:** The project is maintained at the minimum level to apply at least fixes.
 * **Takeover:** The project is currently being taked over by another team and will possibly move.
 * **Not maintained:** The project is not maintained any more (ready to take over).

*Feature requests are welcomed all the time, of course! ;-)*


## License
This project is free under the terms of the AGPL 3.0 (or later) license. 
For more details please see the LICENSE file or: [GNU](https://www.gnu.org/licenses/agpl.html)


## Credits
Please check the git commit history for further details.
